<div align="center">
  <h1>encre-css-website</h1>

  <a href="https://gitlab.com/encre-org/encre-css-website/blob/main/LICENSE">
    <img alt="MIT License" src="https://img.shields.io/badge/license-MIT-success" />
  </a>

  <a href="https://gitlab.com/encre-org/encre-css-website/-/pipelines">
    <img alt="Pipeline status" src="https://gitlab.com/encre-org/encre-css-website/badges/main/pipeline.svg" />
  </a>

  <a href="https://encrecss.uk.to">
    <img alt="Website url" src="https://img.shields.io/website.svg?down_color=red&down_message=down&up_color=green&up_message=up&url=https%3A%2F%2Fencrecss.uk.to" />
  </a>
</div>

### Techs used

The website is written using [`pochoir`](https://gitlab.com/encre-org/pochoir) templates.
The pages are stored in `src/pages` and the components used in pages are stored in `src/components`. All the other assets are stored in the `static` directory.
The styles are generated using [`encre-css`](https://gitlab.com/encre-org/encre-css).
The whole build process is handled by Cargo using a [`custom build script`](https://doc.rust-lang.org/cargo/reference/build-scripts.html).

### Getting started

When developing you can use

```
cargo watch --exec build --ignore target --ignore public
```

to launch a build process which automatically rebuilds on change (you need to install [`cargo-watch`](https://crates.io/crates/cargo-watch))
